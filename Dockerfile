FROM alpine:latest

# Update
RUN apk update
RUN apk upgrade

# Install perl and wget
RUN apk add --no-cache perl wget

# Download texlive
RUN wget --directory-prefix=/tmp http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz

# Prepare install folder
RUN mkdir -p /tmp/install-tl-unx

# Extract texlive
RUN tar -C /tmp/install-tl-unx --strip-components=1 -xzf /tmp/install-tl-unx.tar.gz

# Copy install profile
COPY texlive.profile /tmp/install-tl-unx/texlive.profile

# Run automatic install
RUN perl /tmp/install-tl-unx/install-tl --profile /tmp/install-tl-unx/texlive.profile

# Install required packages
RUN tlmgr update --self --all --no-auto-install
RUN tlmgr install \
amsfonts \
colortbl \
enumitem \
eso-pic \
extsizes \
fontawesome \
fontspec \
geometry \
hyperref \
hyphenat \
infwarerr \
kvoptions \
latex-bin \
listofitems \
luatex \
pdftexcmds \
plex \
plex-otf \
readarray \
tools \
verbatimbox \
xcolor \
xkeyval

RUN tlmgr path add

# Cleanup
RUN rm -rfv /tmp/*

# Setup environment
ENV HOME /data
WORKDIR /data
VOLUME ["/data"]
